# cron-worker

Lightweight alpine-based Docker container with curl and jq utilities to be used for MorphoSource cron tasks.